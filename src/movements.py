from datetime import datetime

class Movement(object):
    def __init__(self, from_acc, to_acc, value, date=None, description=None, currency_code='EUR'):
        self.from_acc = from_acc
        self.to_acc = to_acc
        self.value = value
        self.description = description
        self.date = date if date else datetime.now()
        self.currency_code = currency_code
