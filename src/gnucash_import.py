from datetime import datetime
from gnucash import Session, Transaction, Split, GncNumeric, ACCT_TYPE_ROOT, ACCT_TYPE_EXPENSE, ACCT_TYPE_BANK, \
    ACCT_TYPE_CASH


class GnuCashImport(object):
    _session = None
    _book = None
    _table = None
    _root_acc = None

    def __init__(self, gnucash_file):
        self._gnucash_file = gnucash_file
        self.start()


    def search_account_by_path(self, root, path):
        acc = root.lookup_by_name(path[0])
        if acc.get_instance() is None:
            raise Exception('Account path {} not found'.format(':'.join(path)))
        if len(path) > 1:
            return self.search_account_by_path(acc, path[1:])
        return acc

    def search_account(self, root, name):
        path = name.split(':')
        return self.search_account_by_path(root, path)

    def add_transaction(self, item):
        from_acc = self.search_account(self._root_acc, str(item.from_acc))
        to_acc = self.search_account(self._root_acc, str(item.to_acc))

        currency_code = str(item.currency_code)
        currency = self._table.lookup('ISO4217', currency_code)
        amount = int(float(item.value.replace(',', '.')) * currency.get_fraction())

        tx = Transaction(self._book)
        tx.BeginEdit()
        tx.SetDateEnteredTS(datetime.now())
        tx.SetDatePostedTS(item.date)
        tx.SetDescription(str(item.description))
        tx.SetCurrency(currency)

        s1 = Split(self._book)
        s1.SetParent(tx)
        s1.SetAccount(to_acc)
        s1.SetValue(GncNumeric(amount, currency.get_fraction()))
        s1.SetAmount(GncNumeric(amount, currency.get_fraction()))

        s2 = Split(self._book)
        s2.SetParent(tx)
        s2.SetAccount(from_acc)
        s2.SetValue(GncNumeric(amount * -1, currency.get_fraction()))
        s2.SetAmount(GncNumeric(amount * -1, currency.get_fraction()))

        tx.CommitEdit()

    def start(self):
        self._session = Session(self._gnucash_file)
        self._book = self._session.book
        self._table = self._book.get_table()
        self._root_acc = self._book.get_root_account()

    def save(self):
        self._session.save()

    def end(self):
        self._session.end()

    def get_cash_and_bank_accounts(self):
        return self._get_accounts_by_types((ACCT_TYPE_BANK, ACCT_TYPE_CASH))

    def get_expense_accounts(self):
        return self._get_accounts_by_types((ACCT_TYPE_EXPENSE,))

    def _get_account_path(self, acc):
        path = acc.name
        parent = acc.get_parent()
        while parent and parent.GetType() != ACCT_TYPE_ROOT:
            path = '{}:{}'.format(parent.name, path)
            parent = parent.get_parent()
        return path

    def _get_accounts_by_types(self, types):
        root_acc = self._root_acc
        descendants = root_acc.get_descendants_sorted()
        accounts = []
        for acc in descendants:
            if acc.GetType() in types:
                accounts.append(self._get_account_path(acc))
        return accounts
