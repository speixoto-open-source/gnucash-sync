import os
import httplib2
from apiclient import discovery
from oauth2client import client
from oauth2client.file import Storage
from oauth2client import tools
from .exceptions import RequestError


# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'GnuCashSync'


class Flags(object):
    noauth_local_webserver = True


flags = Flags()


class GoogleClient(object):
    STATE_PROCESS = 'SYNC'

    SYNC_SHEET_TITLE = 'GnuCashSyncSheet'

    def __init__(self, from_accs, to_accs, retries=1):
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())

        self.batch_requests = dict()
        self.retries = retries

        self.sheetService = discovery.build('sheets', 'v4', http=http)
        self.driveService = discovery.build('drive', 'v3', http=http)

        self._from_accs = from_accs
        self._to_accs = to_accs

        self._spreadsheets = []
        self._fetch_sheets()
        self._spreadsheet_id = self.get_sync_sheet_id()
        self._properties_per_sheet_title = self.get_properties_per_sheet_title()


    def get_credentials(self):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir, 'sheets.gnucashsync.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            if flags:
                credentials = tools.run_flow(flow, store, flags)
            else:  # Needed only for compatibility with Python 2.6
                credentials = tools.run(flow, store)
            print('Storing credentials to ' + credential_path)
        return credentials

    def _fetch_sheets(self):
        request = self.driveService.files().list(corpora='user', pageSize=500,
                                                 fields="files(id, name)",
                                                 q="mimeType='application/vnd.google-apps.spreadsheet'")
        results = self._execute_request(None, request, False)
        try:
            results = results['files']
        except KeyError:
            results = []
        self._spreadsheets = results

    def _execute_request(self, spreadsheet_id, request, batch):
        """Execute the request"""
        if batch:
            try:
                self.batch_requests[spreadsheet_id].append(request)
            except KeyError:
                self.batch_requests[spreadsheet_id] = [request]
        else:
            for i in range(self.retries):
                try:
                    response = request.execute()
                except Exception as e:
                    if str(e).find('timed out') == -1:
                        raise
                    if i == self.retries-1:
                        raise RequestError("Timeout")
                else:
                    return response

    def get_sync_sheet_id(self):
        title = self.SYNC_SHEET_TITLE
        try:
            sheet_id = [x['id'] for x in self._spreadsheets if x["name"] == title][0]
            return sheet_id
        except IndexError:
            return self._create_sync_sheet()

    def _create_sync_sheet(self):
        sheets = []
        for from_acc in self._from_accs:
            sheets.append({'properties': {'title': from_acc}})
        body = {
            'properties': {'title': self.SYNC_SHEET_TITLE},
            'sheets': sheets
        }
        response = self.sheetService.spreadsheets().create(body=body).execute()
        sheet_id = response['spreadsheetId']
        values = []
        for to_acc in self._to_accs:
            values.append([to_acc])
        body = {
            'values': values
        }
        for from_acc in self._from_accs:
            r = '{}!A:A'.format(from_acc)
            self.sheetService.spreadsheets().values().update(
                spreadsheetId=sheet_id, range=r, valueInputOption='RAW', body=body).execute()
        return sheet_id

    def get_properties_per_sheet_title(self):
        sheet_id = self._spreadsheet_id
        response = self.sheetService.spreadsheets().get(spreadsheetId=sheet_id).execute()
        properties_per_sheet_title = {}
        for sheet in response.get('sheets', []):
            properties = sheet.get('properties')
            title = properties.get('title')
            properties_per_sheet_title[title] = properties
        return properties_per_sheet_title


    def get_source_accounts(self):
        return self._properties_per_sheet_title.keys()

    def get_movements(self, source_account):
        if source_account not in self._properties_per_sheet_title.keys():
            return []
        sheet_id = self._spreadsheet_id
        columns = self._properties_per_sheet_title[source_account]['gridProperties']['columnCount']
        r = source_account + '!A:' + self._get_column_string(columns)

        response = self.sheetService.spreadsheets().values().get(spreadsheetId=sheet_id, range=r).execute()
        return response.get('values')

    def clear_movements(self, source_account):
        if source_account not in self._properties_per_sheet_title.keys():
            return
        sheet_id = self._spreadsheet_id
        columns = self._properties_per_sheet_title[source_account]['gridProperties']['columnCount']
        r = source_account + '!B:' + self._get_column_string(columns)
        self.sheetService.spreadsheets().values().clear(spreadsheetId=sheet_id,range=r,body={}).execute()

    @staticmethod
    def _get_column_string(columns):
        string = ''
        while columns > 0:
            columns, remainder = divmod(columns - 1, 26)
            string = chr(65 + remainder) + string
        return string

