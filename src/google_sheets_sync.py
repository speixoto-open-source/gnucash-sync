import argparse
from .google_client import GoogleClient
from .gnucash_import import GnuCashImport
from .movements import Movement

def main(args):
    """Shows basic usage of the Sheets API.

    Creates a Sheets API service object and prints the names and majors of
    students in a sample spreadsheet:
    https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
    """
    gimport = GnuCashImport(args.gnucash_file)
    cash_and_bank_accs = gimport.get_cash_and_bank_accounts()
    expenses_accs = gimport.get_expense_accounts()

    client = GoogleClient(cash_and_bank_accs, expenses_accs)

    for acc in cash_and_bank_accs:
        movements_list = client.get_movements(acc)
        for row in movements_list:
            to_acc = row[0]
            values = row[1:]
            for value in values:
                movement = Movement(acc, to_acc, value, description='Imported using GnuCashSync')
                gimport.add_transaction(movement)
        client.clear_movements(acc)
    gimport.save()
    gimport.end()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Google Sheets Sync to GnuCash")
    parser.add_argument('-f', '--gnucash-file', help='Gnucash data file', required=True)

    args = parser.parse_args()
    main(args)