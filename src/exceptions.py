class GnuCashSyncException(Exception):
    """A base class for GnuCash exceptions."""



class RequestError(GnuCashSyncException):
    """Error while sending API request."""