FROM python:2.7-slim

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt \
    && apt update && apt install -y python-gnucash \
	&& rm -rf /var/lib/apt/lists/* \
	&& mv /usr/lib/python2.7/dist-packages/gnucash /usr/local/lib/python2.7/site-packages